using System;
using UnityEditor;
using UnityEngine;

namespace TidakJelas
{
    public static class GuidGenerator
    {
        [MenuItem("Tools/GuidGenerator/Generate")]
        public static void Generate()
        {
            var guid = Guid.NewGuid();
            GUIUtility.systemCopyBuffer = guid.ToString();
            Debug.Log($"Successfully generated new GUID: {guid} at clipboard.");
        }
    }
}
