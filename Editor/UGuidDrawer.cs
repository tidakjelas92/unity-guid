using System;
using UnityEngine;
using UnityEditor;

namespace TidakJelas
{
    [CustomPropertyDrawer(typeof(UGuid))]
    public class UGuidDrawer : PropertyDrawer
    {
        private static RectOffset _boxPadding = EditorStyles.helpBox.padding;
        private static float _lineHeight = EditorGUIUtility.singleLineHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var stringGuidProperty = property.FindPropertyRelative("stringGuid");

            EditorGUI.BeginProperty(position, label, property);

            GUI.Box(EditorGUI.IndentedRect(position), GUIContent.none, EditorStyles.helpBox);
            position = _boxPadding.Remove(position);

            var sceneControlId = GUIUtility.GetControlID(FocusType.Passive);
            position = EditorGUI.PrefixLabel(position, sceneControlId, label);

            position.height = _lineHeight;
            EditorGUI.PropertyField(position, stringGuidProperty, GUIContent.none);

            GUIContent iconContent;
            string message;
            position.y += _lineHeight;
            var iconRect = position;
            iconRect.width = 20.0f;

            var isValid = IsGuidValid(stringGuidProperty.stringValue);
            if (isValid)
            {
                iconContent = EditorGUIUtility.IconContent("d_winbtn_mac_max");
                message = "Guid is valid!";
            }
            else
            {
                iconContent = EditorGUIUtility.IconContent("d_winbtn_mac_close");
                message = "Guid is invalid!";
            }

            EditorGUI.PrefixLabel(iconRect, sceneControlId + 1, iconContent);
            position.width -= iconRect.width;
            position.x += iconRect.width;
            EditorGUI.LabelField(position, message);

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            _boxPadding.vertical + 2.0f * _lineHeight;

        private bool IsGuidValid(string stringGuid)
        {
            Guid guid;
            return Guid.TryParse(stringGuid, out guid);
        }
    }
}
