# unity-shaderkit

Wrapper to allow Unity to serialize System.Guid in editor.

Current Unity version : Unity 2022.3.0f1

Compatibility with other Unity version is unknown, it is advisable to use the current Unity version this project is using or newer.
