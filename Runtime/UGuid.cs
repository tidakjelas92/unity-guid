using System;

namespace TidakJelas
{
    /// <summary>
    /// Wrapper to allow Unity to serialize System.Guid in editor.
    /// </summary>
    /// <remarks>
    /// Will try to return System.Guid wherever possible.
    /// </remarks>
    [Serializable]
    public struct UGuid
    {
        public string stringGuid;
        public Guid guid;

        public UGuid(Guid guid)
        {
            this.guid = guid;
            stringGuid = guid.ToString();
        }

        public UGuid(string guid)
        {
            this.guid = new Guid(guid);
            stringGuid = guid;
        }

        public static implicit operator string(UGuid uGuid) => uGuid.guid.ToString();

        public static implicit operator Guid(UGuid uGuid) => new Guid(uGuid.stringGuid);

        public static bool operator ==(UGuid uGuid1, UGuid uGuid2) => uGuid1.guid == uGuid2.guid;

        public static bool operator !=(UGuid uGuid1, UGuid uGuid2) => uGuid1.guid != uGuid2.guid;

        public override bool Equals(object obj) => guid.Equals((UGuid)obj);

        public override int GetHashCode() => guid.GetHashCode();
    }
}
