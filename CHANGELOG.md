# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2022-07-14

### Deprecated

- UGuid.isValid is deprecated, validity check is now performed in OnGUI

## [0.1.0] - 2022-07-13

### Added

- UGuid, UGuidDrawer
- GuidGenerator

[unreleased]: https://gitlab.com/tidakjelas92/unity-guid/-/compare/v0.2.0...main
[0.1.0]: https://gitlab.com/tidakjelas92/unity-guid/-/tags/v0.1.0
[0.2.0]: https://gitlab.com/tidakjelas92/unity-guid/-/tags/v0.2.0
